
## 0.3.5 [06-07-2023]

* Patch/adapt 2334

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!8

---

## 0.3.4 [04-05-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!9

---

## 0.3.3 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!9

---

## 0.3.2 [08-11-2022]

* minor/ADAPT-2326

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!7

---

## 0.3.1 [05-28-2022]

* Patch: Migrate the adapter to the latest foundation - CALLS.md,...

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!6

---

## 0.3.0 [05-19-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!5

---

## 0.2.0 [01-21-2022]

- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!4

---

## 0.1.3 [08-03-2021]

- Fix the authentication - not able to find token

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!3

---

## 0.1.2 [07-13-2021]

- Remove update files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!1

---

## 0.1.1 [03-02-2021]

- Initial Commit

See commit 83b293e

---
