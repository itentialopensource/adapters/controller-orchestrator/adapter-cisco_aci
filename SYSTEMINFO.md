# Adapter for Cisco ACI

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Application Centric Infrastructure (ACI)
Product Page: https://www.cisco.com/site/us/en/products/networking/cloud-networking/application-centric-infrastructure/index.html

## Introduction
We classify Cisco ACI into the Data Center domain as Cisco ACI provides the ability to manage and make changes to data center network infrastructure.

"Cisco ACI simplifies and automates operations across multisite, multicloud data center networks" 

## Why Integrate
The Cisco ACI adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco ACI. With this adapter you have the ability to perform operations such as:

- Single point of provisioning either via GUI or via REST API.
- Connectivity for physical and virtual workloads with complete visibility on virtual machine traffic.
- Hypervisors compatibility and integration without the need to add software to the hypervisor.
- Capability to create portable configuration templates.
- Capability to insert and automate firewall, load balancers and other L4-7 services.
- Intuitive and easy configuration process.

## Additional Product Documentation
The [API documents for Cisco ACI](https://developer.cisco.com/docs/aci/)