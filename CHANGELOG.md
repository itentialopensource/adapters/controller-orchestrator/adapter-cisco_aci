
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:50PM

See merge request itentialopensource/adapters/adapter-cisco_aci!24

---

## 0.5.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_aci!22

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_19:08PM

See merge request itentialopensource/adapters/adapter-cisco_aci!21

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_20:22PM

See merge request itentialopensource/adapters/adapter-cisco_aci!20

---

## 0.5.0 [07-15-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!19

---

## 0.4.7 [03-28-2024]

* Changes made at 2024.03.28_13:43PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!18

---

## 0.4.6 [03-21-2024]

* Changes made at 2024.03.21_14:31PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!17

---

## 0.4.5 [03-13-2024]

* Changes made at 2024.03.13_11:10AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!16

---

## 0.4.4 [03-11-2024]

* Changes made at 2024.03.11_16:00PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!15

---

## 0.4.3 [02-28-2024]

* Changes made at 2024.02.28_11:29AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!14

---

## 0.4.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!13

---

## 0.4.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!12

---

## 0.4.0 [11-10-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!11

---

## 0.3.5 [06-07-2023]

* Patch/adapt 2334

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!8

---

## 0.3.4 [04-05-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!9

---

## 0.3.3 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!9

---

## 0.3.2 [08-11-2022]

* minor/ADAPT-2326

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!7

---

## 0.3.1 [05-28-2022]

* Patch: Migrate the adapter to the latest foundation - CALLS.md,...

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!6

---

## 0.3.0 [05-19-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!5

---

## 0.2.0 [01-21-2022]

- Migration to the latest foundation and broker ready
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!4

---

## 0.1.3 [08-03-2021]

- Fix the authentication - not able to find token

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!3

---

## 0.1.2 [07-13-2021]

- Remove update files

See merge request itentialopensource/adapters/controller-orchestrator/adapter-cisco_aci!1

---

## 0.1.1 [03-02-2021]

- Initial Commit

See commit 83b293e

---
