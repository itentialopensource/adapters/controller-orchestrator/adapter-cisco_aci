# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Cisco_aci System. The API that was used to build the adapter for Cisco_aci is usually available in the report directory of this adapter. The adapter utilizes the Cisco_aci API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Cisco ACI adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco ACI. With this adapter you have the ability to perform operations such as:

- Single point of provisioning either via GUI or via REST API.
- Connectivity for physical and virtual workloads with complete visibility on virtual machine traffic.
- Hypervisors compatibility and integration without the need to add software to the hypervisor.
- Capability to create portable configuration templates.
- Capability to insert and automate firewall, load balancers and other L4-7 services.
- Intuitive and easy configuration process.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
