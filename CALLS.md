## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco ACI. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco ACI.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for Cisco_aci. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeDomains(callback)</td>
    <td style="padding:15px">get bridge domains</td>
    <td style="padding:15px">{base_path}/{version}/api/class/fvBD.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeDomainsJSON(callback)</td>
    <td style="padding:15px">get bridge domains</td>
    <td style="padding:15px">{base_path}/{version}/api/class/fvBD.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBridgeDomainDetailsJSON(queryTargetFilter, callback)</td>
    <td style="padding:15px">Get Bridge Domain Details</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/fvBD.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchProfiles(callback)</td>
    <td style="padding:15px">get switch profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/class/infraNodeP.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwitchProfilesJSON(callback)</td>
    <td style="padding:15px">get switch profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/class/infraNodeP.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfacePolicies(callback)</td>
    <td style="padding:15px">get interface policies</td>
    <td style="padding:15px">{base_path}/{version}/api/class/infraPortS.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfacePoliciesJSON(callback)</td>
    <td style="padding:15px">get interface policies</td>
    <td style="padding:15px">{base_path}/{version}/api/class/infraPortS.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceProfiles(callback)</td>
    <td style="padding:15px">get interface profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/class/infraProfile.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceProfilesJSON(callback)</td>
    <td style="padding:15px">get interface profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/class/infraProfile.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCoopEpDb(pod, node, domOverlay, callback)</td>
    <td style="padding:15px">get coop-ep-db</td>
    <td style="padding:15px">{base_path}/{version}/topology/{pathv1}/{pathv2}/sys/coop/inst/{pathv3}/coopEpDb.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCoopEpDbJSON(pod, node, domOverlay, callback)</td>
    <td style="padding:15px">get coop-ep-db</td>
    <td style="padding:15px">{base_path}/{version}/topology/{pathv1}/{pathv2}/sys/coop/inst/{pathv3}/coopEpDb.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantXHealthScore(tenant, queryTarget, rspSubtreeInclude, callback)</td>
    <td style="padding:15px">get Tenant X Health Score</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/{pathv1}.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantXHealthScoreJSON(tenant, queryTarget, rspSubtreeInclude, callback)</td>
    <td style="padding:15px">get Tenant X Health Score</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantXStatistics(tenant, callback)</td>
    <td style="padding:15px">get Tenant X Statistics</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFaultsforLeaf(pod, node, queryTarget, rspSubtreeInclude, callback)</td>
    <td style="padding:15px">get Faults for Leaf</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/topology/{pathv1}/{pathv2}.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFaultsforLeafJSON(pod, node, queryTarget, rspSubtreeInclude, callback)</td>
    <td style="padding:15px">get Faults for Leaf</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/topology/{pathv1}/{pathv2}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLeafInterfaceStats(pod, node, iface, callback)</td>
    <td style="padding:15px">get Leaf Interface Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/topology/{pathv1}/{pathv2}/sys/{pathv3}/HDeqptIngrTotal5min-0.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLeafInterfaceStatsJSON(pod, node, iface, callback)</td>
    <td style="padding:15px">get Leaf Interface Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/topology/{pathv1}/{pathv2}/sys/{pathv3}/HDeqptIngrTotal5min-0.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ePGThroughputStats(tenant, queryTarget, rspSubtreeInclude, callback)</td>
    <td style="padding:15px">EPG Throughput Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/{pathv1}/ap-Two-Server-ANP/epg-Server1-epg.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ePGThroughputStatsJSON(tenant, queryTarget, rspSubtreeInclude, callback)</td>
    <td style="padding:15px">EPG Throughput Stats</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/{pathv1}/ap-Two-Server-ANP/epg-Server1-epg.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLLDPNeighbors(pod, node, iface, callback)</td>
    <td style="padding:15px">get LLDP Neighbors</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/topology/{pathv1}/{pathv2}/sys/lldp/inst/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLLDPNeighborsJSON(pod, node, iface, callback)</td>
    <td style="padding:15px">get LLDP Neighbors</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/topology/{pathv1}/{pathv2}/sys/lldp/inst/{pathv3}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPICPhysicalInterfaces(callback)</td>
    <td style="padding:15px">get APIC Physical Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/cnwPhysIf.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPICPhysicalInterfacesJSON(callback)</td>
    <td style="padding:15px">get APIC Physical Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/cnwPhysIf.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPICAggregateInterfaces(callback)</td>
    <td style="padding:15px">get APIC Aggregate Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/cnwAggrIf.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPICAggregateInterfacesJSON(callback)</td>
    <td style="padding:15px">get APIC Aggregate Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/cnwAggrIf.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPICLayer3ManagementInterfaces(callback)</td>
    <td style="padding:15px">get APIC Layer3 Management Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/l3Inst.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAPICLayer3ManagementInterfacesJSON(callback)</td>
    <td style="padding:15px">get APIC Layer3 Management Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/l3Inst.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCtx(callback)</td>
    <td style="padding:15px">get CTX</td>
    <td style="padding:15px">{base_path}/{version}/api/class/fvCtx.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenants(callback)</td>
    <td style="padding:15px">Get Tenants</td>
    <td style="padding:15px">{base_path}/{version}/api/class/fvTenant.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantDetails(queryTargetFilter, callback)</td>
    <td style="padding:15px">Get Tenant Details</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/fvTenant.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenantDetailsJSON(queryTargetFilter, callback)</td>
    <td style="padding:15px">Get Tenant Details</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/fvTenant.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLocalUser(body, callback)</td>
    <td style="padding:15px">add local user</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLocalUserJSON(body, callback)</td>
    <td style="padding:15px">add local user</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInterfacePolicy10GE(body, callback)</td>
    <td style="padding:15px">add interface policy 10GE</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni/infra.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAccessPolicy(body, callback)</td>
    <td style="padding:15px">add aaep</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/infra.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTenant(body, callback)</td>
    <td style="padding:15px">add tenant</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPhysicalDomain(body, callback)</td>
    <td style="padding:15px">add physical domain</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVMwareIntegrationPolicies(body, callback)</td>
    <td style="padding:15px">add VMware integration policies</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.xml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addVMwareIntegrationPoliciesJSON(body, callback)</td>
    <td style="padding:15px">add VMware integration policies</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addProvidedContract(tenant, body, callback)</td>
    <td style="padding:15px">Add Provided Contract</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/{pathv1}/ap-T1_AppProfile/epg-App_Servers.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addConsumedContract(tenant, body, callback)</td>
    <td style="padding:15px">Add Consumed Contract</td>
    <td style="padding:15px">{base_path}/{version}/api/node/mo/uni/{pathv1}/ap-T1_AppProfile/epg-Web_Servers.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenant(tenant, callback)</td>
    <td style="padding:15px">delete tenant</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni/{pathv1}.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addApplicationProfile(body, callback)</td>
    <td style="padding:15px">add application profile</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEndpointGroup(body, callback)</td>
    <td style="padding:15px">add endpoint group (EPG)</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addBridgeDomain(body, callback)</td>
    <td style="padding:15px">add bridge domain (BD)</td>
    <td style="padding:15px">{base_path}/{version}/api/mo/uni.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationProfilesJSON(callback)</td>
    <td style="padding:15px">Get Application Profiles</td>
    <td style="padding:15px">{base_path}/{version}/api/class/fvAp.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplicationProfileDetailsJSON(queryTargetFilter, callback)</td>
    <td style="padding:15px">Get Application Profile Details</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/fvAp.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndpointGroupsJSON(callback)</td>
    <td style="padding:15px">Get Endpoint Groups</td>
    <td style="padding:15px">{base_path}/{version}/api/class/fvAEPg.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndpointGroupDetailsJSON(queryTargetFilter, callback)</td>
    <td style="padding:15px">Get Endpoint Group Details</td>
    <td style="padding:15px">{base_path}/{version}/api/node/class/fvAEPg.json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
